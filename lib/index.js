"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const lit_html_1 = require("lit-html");
const jss_1 = require("jss");
const jss_props_sort_1 = require("jss-props-sort");
const jss_nested_1 = require("jss-nested");
const jss_default_unit_1 = require("jss-default-unit");
const jss_vendor_prefixer_1 = require("jss-vendor-prefixer");
function cls(...names) {
    return names.join(' ');
}
exports.cls = cls;
const jss = jss_1.create({
    plugins: [
        jss_nested_1.default(),
        jss_default_unit_1.default(),
        jss_vendor_prefixer_1.default(),
        jss_props_sort_1.default()
    ],
    virtual: true
});
jss.use({
    onProcessRule(rule) {
        if (rule.type !== 'style' || rule.key.indexOf(':host') !== 0) {
            return rule;
        }
        rule.selector = rule.key;
    }
});
const cache = new WeakMap();
function jssSheet(sheet) {
    return lit_html_1.directive((part) => {
        if (part.value)
            return;
        if (!cache.has(sheet)) {
            const styleElement = document.createElement('style');
            styleElement.innerHTML = sheet.toString();
            cache.set(sheet, styleElement);
        }
        const style = cache.get(sheet);
        part.setValue(document.importNode(style, true));
    });
}
exports.jssSheet = jssSheet;
function createStyleSheet(rules) {
    return jss.createStyleSheet(rules);
}
exports.createStyleSheet = createStyleSheet;
